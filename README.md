# 来客电商

#### QQ交流群
340645969  [点击加入](http://shang.qq.com/wpa/qunwpa?idkey=427109459854834986069455266c718998467b63c78f455940d6291de01a7d0b)

微信小程序电商平台（前后端开源PHP），包含分销，拼团，抽奖，红包，九宫格(老虎机)游戏等功能，整个系统架构非常简单，适合小型团队或者个人开发者二次开发。

小程序 + APP + 公众号 + PC + 生活号

注重界面美感与用户体验，打造独特电商系统生态圈

####  **最新版体验后台** ：[http://www.laiketui.com/action/](http://www.laiketui.com/action/)

#### 软件架构

PHP5.6+ MYSQL5.5+ 自主研发框架

#### 安装教程

http://www.laiketui.com

#### 来客电商微信小程序客户端截图(扫码体验效果更好！！！！)

![扫码体验](https://images.gitee.com/uploads/images/2019/0418/094953_2048c86e_2029865.jpeg)

#### 小程序端截图(新款皮肤)
![首页](https://images.gitee.com/uploads/images/2019/0418/094952_742088f7_2029865.png "在这里输入图片标题")
![会员中心](https://images.gitee.com/uploads/images/2019/0418/094947_553d0c87_2029865.png "在这里输入图片标题")

#### 小程序端截图(稳定版皮肤)
![稳定版皮肤](https://static.oschina.net/uploads/space/2018/1012/174754_S0De_3902514.png "在这里输入图片标题")


#### 后台截图
![输入图片说明](https://images.gitee.com/uploads/images/2018/1108/195946_1f1f0ad0_2029865.png "2018102604085011.png")


#### 功能列表 
1. 产品管理（产品分类管理、产品品牌管理、产品列表管理）
2. 订单管理（订单列表、评价管理、退货管理、订单设置、打印设置）
3. 用户管理（用户列表、用户信息修改）
4. 插件管理（插件列表、拼团活动、抽奖活动）
5. 财务管理（提现申请、提现列表、充值列表）
6. 优惠券管理（优惠券活动、优惠券列表）
7. 签到管理（签到活动、签到记录）
8. 拆红包管理（活动列表、拆红包记录）
9. 砍价管理（砍价商品、砍价记录）
10. 轮播图管理
11. 新闻管理（新闻分类、新闻列表）
12. 页面管理
13. 公告管理（发布公告、公告列表、消息公告）
14. 系统管理（系统参数配置、推广图设置、热门关键词、管理员列表、新增管理员、权限设置）
15. 拼团活动（发布活动、活动列表）
16. 抽奖管理（发布活动、开奖管理）


#### 安装教程

官网查看：[http://www.laiketui.com/](http://www.laiketui.com/)

#### 使用说明

官网查看：[http://www.laiketui.com/help/](http://www.laiketui.com/help/)

#### QQ交流群
340645969


#### 安装部署教程
官网查看：[http://www.laiketui.com/docs/%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3/%E5%AE%89%E8%A3%85%E6%95%99%E7%A8%8B-2/](http://www.laiketui.com/docs/%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3/%E5%AE%89%E8%A3%85%E6%95%99%E7%A8%8B-2/)

#### 开发文档
官网查看：[http://www.laiketui.com/docs/%E5%BC%80%E5%8F%91%E6%96%87%E6%A1%A3/](http://www.laiketui.com/docs/%E5%BC%80%E5%8F%91%E6%96%87%E6%A1%A3/)

#### 相关思维导图
![前端](https://images.gitee.com/uploads/images/2018/1101/171831_05d3ecb6_2029865.jpeg "来客小程序.jpg")

![后台](https://images.gitee.com/uploads/images/2018/1101/171848_6ec689ed_2029865.jpeg "来客后台.jpg")



